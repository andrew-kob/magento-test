<?php
/**
 * Created by PhpStorm.
 * User: ank
 * Date: 13.08.2017
 * Time: 16:55
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$baseEntityType        = 'customer';
$authHashAttributeCode = 'customer_auth_code';
$entityTypeId          = $setup->getEntityTypeId($baseEntityType);
$attributeSetId        = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId      = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$setup->addAttribute($baseEntityType,
                     $authHashAttributeCode,
                     array(
                         'input'            => 'text',
                         'type'             => 'varchar',
                         'label'            => 'Customer Login Code',
                         'visible'          => true,
                         'required'         => false,
                         'user_defined'     => false,
                         'searchable'       => false,
                         'filterable'       => false,
                         'comparable'       => false,
                         'visible_on_front' => false,
                         'unique'           => true,
                         'group'            => 'account',
                     ));

$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    $authHashAttributeCode,
    '999'
);

$oAttribute = Mage::getSingleton('eav/config')->getAttribute($baseEntityType, $authHashAttributeCode);
$oAttribute->setData('used_in_forms', array('adminhtml_customer'));
$oAttribute->save();

$installer->endSetup();