<?php
/**
 * Created by PhpStorm.
 * User: ank
 * Date: 13.08.2017
 * Time: 19:24
 */

//Controllers are not autoloaded so we will have to do it manually:
require_once Mage::getModuleDir('controllers', 'Mage_Customer') . DS . 'AccountController.php';

class Test_AuthByCode_AccountController extends Mage_Customer_AccountController
{
    /**
     * Login post action rewrite done, we need to change default behaviour
     */
    public function loginPostAction()
    {
        // if "login with code" logic is turned on, we use slightly modified parent method code
        if (Mage::getStoreConfig('authbycode_options/section_one/enabled')) {
            if (!$this->_validateFormKey()) {
                $this->_redirect('*/*/');

                return;
            }

            if ($this->_getSession()->isLoggedIn()) {
                $this->_redirect('*/*/');

                return;
            }
            $session = $this->_getSession();

            if ($this->getRequest()->isPost()) {
                $login = $this->getRequest()->getPost('login');
                //START============= original parent code rewrites starts here =============//
                if (!empty($login['authCode']) && !empty($login['password'])) {
                    try {
                        // getting customer by login code in current scope
                        $collection = Mage::getModel('customer/customer')
                                          ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                                          ->getCollection()
                                          ->addFieldToFilter('customer_auth_code', $login['authCode']);
                        $collection->getSelect()->limit(1);

                        /** @var $customer Mage_Customer_Model_Customer */
                        $customer = $collection->load()->getFirstItem();

                        if (!$customer->getEmail()) {
                            throw new Mage_Core_Exception($this->__('No user found with such Auth Code.'));
                        }

                        // email is mandatory, so we can use it to make less changes in original code
                        $session->login($customer->getEmail(), $login['password']);
                        //END============= original parent code rewrites ends here =============//

                        if ($session->getCustomer()->getIsJustConfirmed()) {
                            $this->_welcomeCustomer($session->getCustomer(), true);
                        }
                    } catch (Mage_Core_Exception $e) {
                        switch ($e->getCode()) {
                            case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                                $value   = $this->_getHelper('customer')->getEmailConfirmationUrl($login['authCode']);
                                $message =
                                    $this->_getHelper('customer')
                                         ->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.',
                                              $value);
                                break;
                            case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                                $message = $e->getMessage();
                                break;
                            default:
                                $message = $e->getMessage();
                        }
                        $session->addError($message);
                        $session->setAuthCode($login['authCode']);
                    } catch (Exception $e) {
                        // Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
                    }
                } else {
                    $session->addError($this->__('Login and password are required.'));
                }
            }

            $this->_loginPostRedirect();
        } else {// "login with code" logic is turned off, so we use default magento login post action
            parent::loginPostAction();
        }
    }
}
