<?php

class Test_AuthByCode_Model_Observer
{
    public function validateUniqueCustomerAuthCode($observer)
    {
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $observer->getCustomer();

        if ($customer->getCustomerAuthCode()) {
            $customersWithCode = Mage::getModel('customer/customer')
                                     ->getCollection()
                                     ->addFieldToFilter('customer_auth_code', $customer->getCustomerAuthCode())
                                     ->addFieldToFilter('website_id', $customer->getWebsiteId())
                                     ->addFieldToFilter('entity_id', array('neq' => $customer->getId()))
                                     ->getSize();

            if ($customersWithCode) {
                throw new Mage_Core_Exception('Customer with such Login Code is already exists!');
            }
        }
    }
}
